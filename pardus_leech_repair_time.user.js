// ==UserScript==
// @name        Leech Repair Time
// @namespace   http://userscripts.xcom-alliance.info/
// @description Give the approximate time until your leech has fully repaired your armour.
// @include     http*://*.pardus.at/overview_ship.php
// @include     http*://*.pardus.at/main.php
// @version     1.1
// @icon        http://userscripts.xcom-alliance.info/leech_repair_time/icon.png
// @updateURL 	http://userscripts.xcom-alliance.info/leech_repair_time/pardus_leech_repair_time.meta.js
// @downloadURL http://userscripts.xcom-alliance.info/leech_repair_time/pardus_leech_repair_time.user.js
// @grant       GM_getValue
// @grant       GM_setValue
// ==/UserScript==

/*
	Visit your ship overview page to set/update the amount of armour that your leech
	repairs every 20 minutes. When you are on the navigation screen, the approximate
	time until fully repaired will display beside your current armour value.

	!! Version History !!
    ---------------------
	20.Feb.2013 -- v1.0 -- Initial release
	21.Feb.2013 -- v1.1 -- Added partial refresh support

*/

function getUniverse() {
	return window.location.host.substr(0, window.location.host.indexOf('.'));
};

if (document.URL.search('pardus.at/overview_ship.php') > -1) {
	var repairAmt = 0;
	var el = document.querySelector('table[width="250"]');
	if (el && el.rows[0].cells[1].innerHTML.indexOf('Space Leech') > -1) {
		var repair_match = el.rows[1].cells[1].textContent.match(/(\d+) point/);
		if (repair_match.length) {
			repairAmt = parseInt(repair_match[1], 10);
		}
	}
	GM_setValue(getUniverse() + 'RepairAmt', repairAmt);
}

else

if (document.URL.search('pardus.at/main.php') > -1) {

	var LeechRepairTime = {
		repairAmt: 0,
		init: function() {
			this.repairAmt = parseInt(GM_getValue(getUniverse() + 'RepairAmt', 0), 10);
		},
		updateRepairEstimate: function() {
			if (document.getElementById('spanShipArmor') && document.getElementById('spanShipArmorMax')) {
				var delta = document.getElementById('spanShipArmorMax').textContent - document.getElementById('spanShipArmor').textContent;
				if (delta > 0 && this.repairAmt > 0) {
					var leechTicks = Math.ceil(delta/this.repairAmt);
					document.getElementById('spanShipArmorMax').parentNode.innerHTML += '<span style="float:right" title="Approximate time until fully repaired by your leech">'+Math.floor(leechTicks/3)+'h '+20*(leechTicks%3)+'m</span>';
				}
			}
		}
	}

	// run normally once
	LeechRepairTime.init();
	LeechRepairTime.updateRepairEstimate();

	// hook in to allow for partial refresh
	unsafeWindow.addUserFunction(function(lrt) {
		return function() { lrt.updateRepairEstimate() };
	}(LeechRepairTime));

}