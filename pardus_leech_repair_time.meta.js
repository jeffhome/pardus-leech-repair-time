// ==UserScript==
// @name        Leech Repair Time
// @namespace   http://userscripts.xcom-alliance.info/
// @description Give the approximate time until your leech has fully repaired your armour.
// @include     http*://*.pardus.at/overview_ship.php
// @include     http*://*.pardus.at/main.php
// @version     1.1
// @icon        http://userscripts.xcom-alliance.info/leech_repair_time/icon.png
// @updateURL 	http://userscripts.xcom-alliance.info/leech_repair_time/pardus_leech_repair_time.meta.js
// @downloadURL http://userscripts.xcom-alliance.info/leech_repair_time/pardus_leech_repair_time.user.js
// @grant       GM_getValue
// @grant       GM_setValue
// ==/UserScript==
